﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASD107;

namespace Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var at = new ArrayTasks();
            var result = at.JoinArray(
                new int[] { 1 },
                new int[] { 3 });
            Assert.AreEqual<int>(result[0], 1);
            Assert.AreEqual<int>(result[1], 3);
        }
        [TestMethod]
        public void CoupleTest()
        {
            var result = ArrayTasks.UniqNumCount(new int[7] { 1, 2, 3, 1, 10, 10, 10 });
            Assert.AreEqual<int>(result.Length, 4);
            Assert.AreEqual<int>(result[0].Key, 1);
            Assert.AreEqual<int>(result[0].Value, 2);
            Assert.AreEqual<int>(result[1].Key, 2);
            Assert.AreEqual<int>(result[1].Value, 1);
            Assert.AreEqual<int>(result[2].Key, 3);
            Assert.AreEqual<int>(result[2].Value, 1);
            Assert.AreEqual<int>(result[3].Key, 10);
            Assert.AreEqual<int>(result[3].Value, 3);
        }
    }
}
