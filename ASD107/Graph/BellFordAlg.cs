﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASD107.Graph
{
    /// <summary>
    /// Не уверена, что работает корректно и вообще работает
    /// </summary>
    public class BellFordAlg
    {
        public void Run()
        { 
        var graph = new (int, int, int)[8]
                {(0,1,-1),
                 (0,2,4),
                 (1,2,3),
                 (1,3,2),
                 (1,4,2),
                 (3,2,5),
                 (3,1,1),
                 (4,3,-3)
                };
            int vershinaCount = 5;
            int[] distances = new int[vershinaCount];
            for (int i = 1; i < vershinaCount; i++)
            {
                distances[i] = Int32.MaxValue;
            }
            for (int i = 1; i<vershinaCount; i++)
            {
                foreach (var edge in graph)
                {
                    if (distances[edge.Item2] > distances[edge.Item1] + edge.Item3)
                        distances[edge.Item2] = distances[edge.Item1] + edge.Item3;
                }
            }
        }
    }
}
