﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASD107
{
    /// <summary>
    /// Решение задачи о рюкзаке жадным алгоритмом
    /// </summary>
    public class BagGreedy
    {
        public void FillBag()
        {
            var items = ReadBagItem();

            //сортировка по удельной ценности, 
            //вещи с одной и той же удельной ценностью 
            //сортируются в порядке возрастания веса
            items = SortByPricePerWeight(items);

            //цикл, в котором заполняем рюкзак
            var bagWeight = Convert.ToInt32(Console.ReadLine());
            var itemIndex = 0; //позиция предмета, который мы кладем в рюкзак
            while (bagWeight > 0 && itemIndex < items.Length) 
            {
                while (items[itemIndex].Weight <= bagWeight)
                {
                    Console.WriteLine($"Положили вещь с весом " +
                        $"{items[itemIndex].Weight}" +
                        $" и стоимостью {items[itemIndex].Price}");
                    bagWeight -= items[itemIndex].Weight;
                }
                itemIndex++;
            }
        }

        /// <summary>
        /// Сортировка по удельному весу слиянием 
        /// </summary>
        private BagItem[] SortByPricePerWeight(BagItem[] array) 
        {
            //если  массив из 1 элемента - возвращаем
            if (array.Length == 1)
                return array;
            //находим середину массива, чтобы поделить на 2 массива
            int middle = array.Length / 2;
            //создаем 2 массива и вызываем сортировку
            BagItem[] leftArray = new BagItem[middle];
            for (int i = 0; i < middle; i++)
                leftArray[i] = array[i];
            BagItem[] rightArray = new BagItem[array.Length - middle];
            for (int i = middle; i < array.Length; i++)
                rightArray[i- middle] = array[i];
            var sortedLeft = SortByPricePerWeight(leftArray);
            var sortedRight = SortByPricePerWeight(rightArray);

            //слияние 2 отсортированнных массивов
            var result = new BagItem[array.Length];
            var resultIndex = 0;
            var leftIndex = 0;
            var rightIndex = 0;
            while (leftIndex < sortedLeft.Length
                && rightIndex < sortedRight.Length)
            {
                if (sortedLeft[leftIndex].PricePerWeight >
                    sortedRight[rightIndex].PricePerWeight)
                {
                    result[resultIndex] = sortedLeft[leftIndex];
                    leftIndex++;
                }
                else 
                {
                    result[resultIndex] = sortedRight[rightIndex];
                    rightIndex++;
                }
                resultIndex++;
            }

            if (leftIndex < sortedLeft.Length) 
            {
                for (int i = leftIndex; i < sortedLeft.Length; i++)
                {
                    result[resultIndex] = sortedLeft[i];
                    resultIndex++;
                }
            }

            if (rightIndex < sortedRight.Length)
            {
                for (int i = rightIndex; i < sortedRight.Length; i++)
                {
                    result[resultIndex] = sortedRight[i];
                    resultIndex++;
                }
            }

            return result;
        }

        private BagItem[] ReadBagItem() 
        {
            return new BagItem[]
                { 
                    new BagItem(15, 15),
                    new BagItem(30, 90),
                    new BagItem(50,100) 
                };
        }
    }

    /// <summary>
    /// Вещь из рюкзака
    /// </summary>
    public class BagItem
    {
        /// <summary>
        /// Вес вещи
        /// </summary>
        public int Weight { get; set; }
        /// <summary>
        /// Стоимость вещи
        /// </summary>
        public int Price { get; set; }
        /// <summary>
        /// Удельная стоимость
        /// </summary>
        public double PricePerWeight { get; set; }
        public BagItem(int weight, int price) 
        {
            if (weight < 0)
                throw new ArgumentException("Вес не может быть меньше нуля");
            if (price < 0)
                throw new ArgumentException("Цена не может быть меньше нуля");

            Weight = weight;
            Price = price;
            PricePerWeight = price / weight;
        }
    }
}
