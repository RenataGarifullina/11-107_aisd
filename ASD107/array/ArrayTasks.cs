﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASD107.array;

namespace ASD107
{
    public class ArrayTasks
    {
        public int[] JoinArray(int[] a1, int[] a2)
        {
            if (a1?.Length == 0 && a2?.Length == 0)
            {
                Console.WriteLine("Массивы пустые");
                return null;
            }

            if (a1?.Length == 0 && a2?.Length > 0)
                return a2;

            if (a1?.Length > 0 && a2?.Length == 0)
                return a1;

            //позиция текущего элемента массива
            int i1 = 0;
            int i2 = 0;
            int[] result = new int[a1.Length + a2.Length];
            int iResult = 0;
            while (i1 < a1.Length && i2 < a2.Length)
            {
                if (a1[i1] < a2[i2])
                {
                    result[iResult] = a1[i1];
                    i1++;
                }
                else 
                {
                    result[iResult] = a2[i2];
                    i2++;
                }
                iResult++;
            }

            if (i1 < a1.Length)
                for (int i = i1; i < a1.Length; i++)
                {
                    result[iResult] = a1[i];
                    iResult++; 
                }

            if (i2 < a2.Length)
                for (int i = i2; i < a2.Length; i++)
                {
                    result[iResult] = a2[i];
                    iResult++;
                }

            return result;
        }
        /// <summary>
        ///  Дан массив из N целых чисел. Необходимо посчитать сколько в нем уникальных значений
        /// </summary>
        /// <returns></returns>
        public static Couple[] UniqNumCount(int[] array)
        {
            if (array == null)
            {
                throw new Exception("Массив пуст!");
            }
            Couple[] couple = new Couple[0];
            foreach (var el in array)
            {
                int i = 0;
                while (i<couple.Length)
                {
                    if (couple[i].Key == el)
                    {
                        couple[i].Value++;
                        break;
                    }
                    i++;
                }
                if (i== couple.Length)
                {
                    Array.Resize(ref couple, couple.Length + 1) ;
                    couple[i] = new Couple(el, 1);
                   
                }
              
            }
            return couple;
        }
    }
}
