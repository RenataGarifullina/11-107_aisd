﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASD107
{
    public class DifferenceArray
    {
        public int[] ArrayDiff(int[] a1, int[] a2)
        {
            if (a1?.Length == 0 && a2?.Length == 0)
            {
                Console.WriteLine("Массивы пустые");
                return null;
            }
            if (a1?.Length > 0 && a2?.Length == 0)
                return a1;
            int i1 = 0;
            int i2 = 0;
            int[] result = new int[a1.Length];
            int iResult = 0;
            while (i1 < a1.Length && i2 < a2.Length)
            {
                if (a1[i1] < a2[i2])
                {
                    result[iResult] = a1[i1];
                    i1++;
                    iResult++;
                }
                else if (a1[i1] == a2[i2])
                {
                    i1++;
                    i2++;
                }
                else if (a1[i1] > a2[i2])
                {
                    i2++;
                }

            }
            while(i1 < a1.Length)
            {
                result[iResult] = a1[i1];
                iResult++;
                i1++;
            }
            return result;
        }
        public int[] Intersection(int[] a1, int[] a2)
        {
            if (a1?.Length == 0 && a2?.Length == 0)
            {
                Console.WriteLine("Массивы пустые");
                return null;
            }

            if (a1?.Length == 0 && a2?.Length > 0)
                return a2;

            if (a1?.Length > 0 && a2?.Length == 0)
                return a1;
            int i1 = 0;
            int i2 = 0;
            int[] result = new int[a1.Length + a2.Length];
            int iResult = 0;
            while (i1 < a1.Length && i2 < a2.Length)
            {
                if (a1[i1] < a2[i2])
                {
                    i1++;
                }
                else if (a1[i1] == a2[i2])
                {
                    result[iResult] = a1[i1];
                    i1++;
                    i2++;
                    iResult++;
                }
                else i2++;
            }
            return result;
        }
    }
}
