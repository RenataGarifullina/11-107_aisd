﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASD107
{
    public static class IntArrayExtension
    {
        public static int[] RemoveAt(
            this int[] array, int position) 
        {
            //Проверяем, есть ли соответствующая позиция
            if (position >= array.Length)
                throw new ArgumentOutOfRangeException("Удаление элемента за пределом массива");

            for (int i = position; i < array.Length - 1; i++)
            {
                array[i] = array[i + 1];
            }

            Array.Resize(ref array, array.Length - 1);

            return array;
        }
    }
}
